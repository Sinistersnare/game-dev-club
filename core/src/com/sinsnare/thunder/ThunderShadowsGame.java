package com.sinsnare.thunder;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class ThunderShadowsGame extends ApplicationAdapter {
	SpriteBatch batch;
	Texture img, background;
	Sound mac;

	int x, y;

	@Override
	public void create() {
		batch = new SpriteBatch();
		img = new Texture("badlogic.jpg");
		background = new Texture("background.jpg");
		mac = Gdx.audio.newSound(Gdx.files.internal("sounds/machine.wav"));
		mac.loop();
		x = 0;
		y = 0;
	}

	@Override
	public void render() {
		Gdx.gl.glClearColor(1, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		if (Gdx.input.isKeyPressed(Keys.RIGHT)) {
			x += 14;
		} else if (Gdx.input.isKeyPressed(Keys.LEFT)) {
			x -= 14;
		} else if (Gdx.input.isKeyPressed(Keys.UP)) {
			y += 14;
		} else if (Gdx.input.isKeyPressed(Keys.DOWN)) {
			y -= 14;
		}

		batch.begin();
		batch.draw(background, 0, 0, Gdx.graphics.getWidth(),
				Gdx.graphics.getHeight());
		batch.draw(img, x, y);
		batch.end();
	}
}
